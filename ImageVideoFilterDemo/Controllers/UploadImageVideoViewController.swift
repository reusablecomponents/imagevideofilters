//
//  UploadImageVideoViewController.swift
//  ImageVideoFilterDemo
//
//  Created by signity on 11/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
class UploadImageVideoViewController: UIViewController {
    
    // And then use the default configuration like so:
    var picker = YPImagePicker()
    @IBOutlet weak var selectedImageV: UIImageView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var uploadPostButton: UIButton!
    @IBOutlet weak var postTitleText: UITextField!
    @IBOutlet weak var postDetailText: UITextView!
    
    var isPickerLoad = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
//        isPickerLoad ? nil : self.showPicker()
        
    }
    @IBAction func uploadPostPicker() {
        if self.validateUploadData() {
            //Call API for upload here
        }
        
    }
    @IBAction func addImagePicker() {
        self.isPickerLoad = true
        var config = YPImagePickerConfiguration()
        config.onlySquareImagesFromLibrary = false
        config.onlySquareImagesFromCamera = true
        config.showsVideoInLibrary = true
        config.libraryTargetImageSize = .original
        config.usesFrontCamera = true
        config.showsFilters = true
        config.shouldSaveNewPicturesToAlbum = true
        config.videoCompression = AVAssetExportPresetHighestQuality
        config.albumName = "FireBaseChatApp"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .photo
        config.videoRecordingTimeLimit = 10
        config.videoFromLibraryTimeLimit = 20
        config.showsCrop = .rectangle(ratio: (16/9))
        // Build a picker with your configuration
         self.picker = YPImagePicker(configuration: config)
        // unowned is Mandatory since it would create a retain cycle otherwise :)
        self.picker.didSelectImage = { [unowned picker] img in
            // image picked
            print(img.size)
             self.selectedImageV.image = img
            picker.dismiss(animated: true, completion: nil)
        }
        self.picker.didSelectVideo = { videoData,videoThumbnailImage, imageUrl in
            // video picked
            self.selectedImageV.image = videoThumbnailImage
            self.picker.dismiss(animated: true, completion: nil)
        }
//        self.picker.didClose = {
//        self.isPickerLoad = false
//        self.picker.dismiss(animated: true, completion: nil)
//        self.tabBarController?.selectedIndex = 0
//        }
        present(self.picker, animated: true, completion: nil)
    }
}
// Support methods
extension UploadImageVideoViewController {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: fabs(size.width), height: fabs(size.height))
    }
}

