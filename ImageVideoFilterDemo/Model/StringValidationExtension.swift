//
//  StringValidationExtension.swift
//  ImageVideoFilterDemo
//
//  Created by Signity on 15/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func isNotValidateEmailId() -> Bool {
        if self.trimmingCharacters(in: .whitespacesAndNewlines)
            .components(separatedBy: "@")[0]
            .components(separatedBy: ".").count > 2 {
            return true
        }
        
        let emailRegex: String = "[A-Z0-9a-z()<>@!#$%&'*+-/=?^_`{}|~.]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return !emailTest.evaluate(with: self.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    //07e5a4f90e32cb91bdb1d9f99804761c
    // Must be between 8 - 16 characters with at least 2 of any of the following capital letter, number, special character e.g. !#.$
    func isNotValidPassword() -> Bool {
        let passwordRegex: String = "^(?=.*[0-9])(?=.*[a-z])|(?=.*[!@#$%^&*+=?-]).{6,16}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return !passwordTest.evaluate(with: self.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    func isNotValidRegistrationNumber() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    func isNumeric() -> Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    func isStringEmpty() -> Bool {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0
    }
    
    func getTrimmedStr() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: range.lowerBound)
        let idx2 = index(startIndex, offsetBy: range.upperBound)
        return String(self[idx1..<idx2])
    }
}
