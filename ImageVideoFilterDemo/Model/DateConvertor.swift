//
//  DateConvertor.swift
//  ImageVideoFilterDemo
//
//  Created by Signity on 10/10/17.
//  Copyright © 2017 Signity. All rights reserved.
//

import UIKit
// swiftlint:disable trailing_whitespace
extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
// MARK: -   All Date Formats 
struct DateFormats {
    static let LocalDateTypeFormat  =  "dd MMM yyyy"
    static let ServerDateTypeFormat =  "yyyy-MM-dd"
    static let DateTypeFormat       =  "dd-MM-yyyy"
    static let TimeAMPMTypeFormat   =  "hh:mm a"
    static let yearTypeFormat       =  "yyyy"
    static let TimeTypeFormat       =  "HH:mm:ss"
    static let ServerDateFormat     =  "yyyy-MM-dd HH:mm:ss"
    static let ServerDateTimeFormat     =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
}
// MARK: -  Date Converters with all formats 
  class DateConvertor: NSObject {
    // MARK: -   Convert String Into Date with any formate 
    class func convertIntoDate(slotDate: String, dateFormat: String) -> Date {
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let sDate                = dateFormatter.date(from: slotDate)!
        return sDate
    }
// MARK: -   Convert Date Into String with any formate 
    class func convertToString(slotDate: Date, dateFormat: String) -> String {
        let dateFormatter        = self.getUTCTimeformatter(dateFormat: dateFormat)
        let sdate                = dateFormatter.string(from: slotDate)
        return sdate
    }
// MARK: -   Convert String Into UTC Date with any formate 
    class func convertToDate(slotDate: String, dateFormat: String) -> Date {
        let dateFormatter        = self.getUTCTimeformatter(dateFormat: dateFormat)
        let sDate                = dateFormatter.date(from: slotDate)!
        return self.getCurrentZoneDateTime(sourceDate: sDate)
    }
// MARK: -   Get UTC Time 
    class func getUTCTimeformatter(dateFormat: String) -> DateFormatter {
        let dateFormatter        = DateFormatter()
        dateFormatter.timeZone   = TimeZone(abbreviation: "UTC")
        dateFormatter.locale     = Locale.init(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter
    }
// MARK: -   Get Current Date Time in UTC Formate 
    class func getCurrentZoneDateTime(sourceDate: Date) -> Date {
        let utcTimeZone          = TimeZone(abbreviation: "UTC")
        let systemTimeZone       = NSTimeZone.system
    
        let utcTimeZoneOffset    =   utcTimeZone?.secondsFromGMT(for: sourceDate)
        let systemTimeZoneOffset =   systemTimeZone.secondsFromGMT(for: sourceDate)
    
        let interval = systemTimeZoneOffset - utcTimeZoneOffset!
        let nowDate = Date.init(timeInterval: TimeInterval(interval), since: sourceDate)
    
        return nowDate
    }
  
    
}
