//
//  GetCameraPhotoLibModel.swift
//  ImageVideoFilterDemo
//
//  Created by Signity on 12/02/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
var selectedFileHandler : ((NSURL?) -> Void)?
var selectedImageHandler : ((UIImage?,String?) -> Void)?
class GetCameraPhotoLibModel: NSObject, UIImagePickerControllerDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    static let shared = GetCameraPhotoLibModel()
    
      // MARK: -  get Camera/Photos alert controller 
     func getCameraPhotos(picker: UIImagePickerController, sender: UIViewController)  {
        picker.delegate = self
        let actionSheetController: UIAlertController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnTakePhoto, style: .default, handler: { action in
           self.openCamera(picker: picker, sender: sender)
        }))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnSelectPhoto, style: .default, handler: { action in
            self.openphotoLibrary(picker: picker, sender: sender, mediaType: [kUTTypeImage as String])
            
        }))
        sender.present(actionSheetController, animated: true, completion: nil)
    }
   
    
    //MARK: -  Open Camera For User Profile Image 
     func openCamera(picker: UIImagePickerController, sender: UIViewController) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            sender.present(picker,animated: true,completion: nil)
        }
    }
    func openphotoLibrary(picker: UIImagePickerController, sender: UIViewController, mediaType: [String]) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            picker.mediaTypes = mediaType // Add media type
            sender.present(picker,animated: true,completion: nil)
        }
    }
    // MARK: -  UIImagePicker Delegates 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        var imageExt = ""
        if #available(iOS 11.0, *) {
            let assetPath = info[UIImagePickerControllerImageURL] as! NSURL
        
        if (assetPath.absoluteString?.hasSuffix("JPG"))! {
            imageExt = "JPG"
            print("JPG")
        }
        else if (assetPath.absoluteString?.hasSuffix("PNG"))! {
            imageExt = "PNG"
            print("PNG")
        }
        else if (assetPath.absoluteString?.hasSuffix("GIF"))! {
            imageExt = "GIF"
            print("GIF")
        }
        else {
            print("Unknown")
        }
        } else {
            // Fallback on earlier versions
        }
        selectedImageHandler!(chosenImage,imageExt)
//        if let urlOfImage = info[UIImagePickerControllerReferenceURL] as? NSURL{
//            // Handle picking a Photo from the Photo Library
//           selectedFileHandler!(urlOfImage)
//        }else {
//            picker.dismiss(animated: true, completion: nil)
//        }
    }
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
