//
//  ConstantModel.swift
//  ImageVideoFilterDemo
//
//  Created by Pooja Rana on 24/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class ConstantModel: NSObject {
    static let ProjectName          = "LoginSignUp Reusable"
    static let appDelegate          = UIApplication.shared.delegate as! AppDelegate
    static let firebaseImageStorageUrl = "gs://fcm-demo-app-25c5f.appspot.com"
    static let api                  = API()
    static let message              = AppMessage()
    static let btnText              = AppButtonText()
}

struct API {
    let ServiceFailure      = "Something went wrong, please try again."
    let WeakInternet        = "Please check your internet connection before using."
    let NoInternet          = "No internet connection."
}

struct AppMessage {
    
    let FillAllField        = "Please fill in all required fields."
    let EnterEmail          = "Please enter email address"
    let ValidEmail          = "Valid email required"
    let ValidPassword       = "Please create a password between 6-16 characters with at least any of the following alphabet, number and allowed special characters e.g. !@#$%^&*+=?-"
    let MatchPassword       = "The passwords you’ve entered don’t match, please review and try again"
    let NoCountry           = "No country found"
    let SelectCountryFirst  = "Please select country first"
}
struct  AppButtonText {
    let btnSelect           = "Please select"
    let btnTakePhoto        = "Take Photo"
    let btnSelectPhoto      = "Select Photo"
    let btnCancel           = "Cancel"
    let btnRetry            = "Retry"
    let btnOk               = "Ok"
    let btnContinue         = "Continue"
    let btnDone             = "Done"
    let btnDelete           = "Delete"
    let btnCopy             = "Copy"
    
}
