//
//  FormValidationModel.swift
//  ImageVideoFilterDemo
//
//  Created by signity on 13/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

extension UploadImageVideoViewController {
    
    func validateUploadData() -> Bool {
        self.view.endEditing(true)
        if postTitleText.text!.isStringEmpty() || postDetailText.text!.isStringEmpty() {
            EventManager.showAlert(alertMessage: ConstantModel.message.FillAllField, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        } else {
            return true 
        }
    }
    
}
